SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


-- -----------------------------------------------------
-- Table `self_hodblog`.`tbl_category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `self_hodblog`.`tbl_category` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `alias` VARCHAR(100) NOT NULL,
  `description` VARCHAR(1000) NULL,
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_title` (`title` ASC),
  INDEX `idx_alias` (`alias` ASC),
  UNIQUE INDEX `alias_UNIQUE` (`alias` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `self_hodblog`.`tbl_article`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `self_hodblog`.`tbl_article` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_id` INT(11) UNSIGNED NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `alias` VARCHAR(100) NOT NULL,
  `content` LONGTEXT NOT NULL,
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_article_category_idx` (`category_id` ASC),
  INDEX `idx_created` (`created` ASC),
  INDEX `idx_title` (`title` ASC),
  INDEX `idx_alias` (`alias` ASC),
  UNIQUE INDEX `alias_UNIQUE` (`alias` ASC),
  CONSTRAINT `FK_article_category`
    FOREIGN KEY (`category_id`)
    REFERENCES `self_hodblog`.`tbl_category` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
