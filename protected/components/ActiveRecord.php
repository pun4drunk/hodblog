<?php

abstract class ActiveRecord extends CActiveRecord {
    
    public function scopes() {
        
        $scopes = array();
        if ($this->hasAttribute('title')) {
            $scopes['abc'] = array(
                'order' => 'title ASC', 
            );
        }
        
        return array_merge($scopes, parent::scopes());
    }
    
    public function beforeValidate() {
        if ($this->hasAttribute('alias')) {
            
            if (empty($this->alias)) {
                $this->alias = preg_replace('/[^0-9a-z-]/', '', mb_strtolower($this->title));
            }
            
            $baseAlias = $this->alias.'';
            $i = 1;
            while (!$this->checkUnique('alias')) {
                $this->alias = $baseAlias.'-'.$i++;
            }
        }
        return parent::beforeValidate();
        
    }
    
    public function uriComponent($attribute, $params) {
        
        $minChars = 3;
        $pattern = "/^[a-z0-9-]{".$minChars.",}$/";
        if (!preg_match($pattern, $this->$attribute)) {
            $this->addError($attribute, $attribute.' must contain letters, numbers and hyphens only and be minimum '.$minChars.' characters in length.');
        }
    }
    
    protected function checkUnique($attribute) {
        $className = get_class($this);
        if ($item = $className::model()->findByAttributes(array(
            $attribute => $this->$attribute,
        ))) {
            if ($this->getPrimaryKey() !== $item->getPrimaryKey()) {
                return false;
            }
        }
        return true;
    }
    
    public function beforeSave() {
        
        $user_id = (int) Yii::app()->user->id;
        
        if($this->hasAttribute('created_by')){
            if ($user_id && $this->isNewRecord) {
                $this->created_by = $user_id;
            } else if (empty($this->created_by)) {
                $this->created_by = NULL;
            }
        }
        
        if($this->hasAttribute('modified_by')){
            if ($user_id && !$this->isNewRecord) {
                $this->modified_by = $user_id;
            } else if (empty($this->modified_by)) {
                $this->modified_by = NULL;
            }
        }    
        
        return parent::beforeSave();
    }
    
    function behaviors(){
        $behaviors = array();
        if($this->hasAttribute('created')){
            $timestamps = array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'created',
            );
            
            if($this->hasAttribute('modified')){
                $timestamps['updateAttribute'] = 'modified';
                $timestamps['setUpdateOnCreate'] = false;
            }
            $behaviors['timestamps'] = $timestamps;
        }
        return array_merge(parent::behaviors(), $behaviors);
    }
    
}
