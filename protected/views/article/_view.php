<?php
/* @var $this ArticleController */
/* @var $data Article */
?>
<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('category_id')); ?>:</b>
	<?php echo CHtml::encode($data->category->title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('modified')); ?>:</b>
	<?php echo $data->modified ? CHtml::encode($data->modified) : 'Never'; ?>
	<br />

        
	<b><?php echo CHtml::encode($data->getAttributeLabel('content')); ?>:</b>
        <?php $this->beginWidget('CHtmlPurifier'); ?>
            <?php echo $data->content ?>
        <?php $this->endWidget(); ?>
	<br />



</div>