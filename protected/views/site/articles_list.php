<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$items = $dataProvider->getData();
?>

<section id="itemsList" class="row-fluid">
    <?php foreach ($items as $model): ?>
        <article id="item-<?php echo $model->getPrimaryKey()?>">
            <h2><?php echo CHtml::encode($model->title); ?></h2>
            <div class="item-details">
                <span class="small">Category: <?php echo CHtml::encode($model->category->title)?></span>
                &nbsp;|&nbsp;
                <span class="small">Created: <?php echo CHtml::encode($model->created)?></span>
            </div>
            <div class="item-content">
                <?php $this->beginWidget('CHtmlPurifier'); ?>
                    <?php echo $model->content ?>
                <?php $this->endWidget(); ?>
            </div>    
        </article>
        <hr>
    <?php endforeach;?>
</section>