<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$categories = CHtml::listData(Category::model()->abc()->findAll(), 'id', 'title');

$divId = "#itemsList";
?>

<div class="form">
    <?php echo CHtml::beginForm() ?>
    <?php //echo CHtml::label('Select Category', 'category_id'); ?>
    <?php echo CHtml::dropDownList('category_id', $category_id, $categories, array(
        'empty' => 'All Categories',
        'ajax' => array(
            'type' => 'POST',
            'update' => $divId,
            'beforeSend' => 'function(){
              $("'.$divId.'").html("&nbsp;").addClass("grid-view-loading");}',
            'complete' => 'function(){
              $("'.$divId.'").removeClass("grid-view-loading");}'
        )
    )) ?>
    <?php /*echo CHtml::ajaxSubmitButton('Show', '', array(
        'type' => 'POST',
        'update' => $divId,
        'beforeSend' => 'function(){
          $("'.$divId.'").html("&nbsp;").addClass("grid-view-loading");}',
        'complete' => 'function(){
          $("'.$divId.'").removeClass("grid-view-loading");}',
    ), array(
        'type' => 'submit'
    )); */ ?>
    <?php echo CHtml::endForm() ?>
</div>
<?php require 'articles_list.php' ?>